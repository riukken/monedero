﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using ProyectoMonedero.Models;

namespace ProyectoMonedero.Controllers
{
    //CORS
    //origen que queremos permitir headers y methods
    [EnableCors(origins: "http://localhost:3000", headers: "*", methods: "*")]
    public class consumosController : ApiController
    {
        private monederoEntities db = new monederoEntities();

        // GET: api/consumoes
        public IQueryable<consumo> Getconsumos()
        {
            return db.consumos;
        }

        // GET: api/consumoes/5
        [ResponseType(typeof(consumo))]
        public IHttpActionResult Getconsumo(int id)
        {
            consumo consumo = db.consumos.Find(id);
            if (consumo == null)
            {
                return NotFound();
            }

            return Ok(consumo);
        }

        // PUT: api/consumoes/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Putconsumo(int id, consumo consumo)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != consumo.idConsumo)
            {
                return BadRequest();
            }

            db.Entry(consumo).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!consumoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/consumoes
        [ResponseType(typeof(consumo))]
        public IHttpActionResult Postconsumo(consumo consumo)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.consumos.Add(consumo);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = consumo.idConsumo }, consumo);
        }

        // DELETE: api/consumoes/5
        [ResponseType(typeof(consumo))]
        public IHttpActionResult Deleteconsumo(int id)
        {
            consumo consumo = db.consumos.Find(id);
            if (consumo == null)
            {
                return NotFound();
            }

            db.consumos.Remove(consumo);
            db.SaveChanges();

            return Ok(consumo);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool consumoExists(int id)
        {
            return db.consumos.Count(e => e.idConsumo == id) > 0;
        }
    }
}