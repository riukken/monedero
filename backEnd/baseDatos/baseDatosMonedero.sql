USE [monedero]
GO
/****** Object:  Table [dbo].[clientes]    Script Date: 28/11/2019 10:45:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[clientes](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](100) NULL,
	[apellidos] [varchar](100) NULL,
	[dni] [varchar](20) NULL,
	[img] [varchar](200) NULL,
	[email] [varchar](100) NULL,
	[passwordUno] [varchar](100) NULL,
	[passwordDos] [varchar](100) NULL,
	[codigoPersonal] [varchar](200) NULL,
	[role] [int] NULL,
 CONSTRAINT [PK_clientes] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[consumos]    Script Date: 28/11/2019 10:45:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[consumos](
	[idConsumo] [int] IDENTITY(1,1) NOT NULL,
	[idUser] [int] NULL,
	[idOferta] [int] NULL,
	[fechaRegistro] [date] NULL,
	[cantidad] [int] NULL,
 CONSTRAINT [PK_consumos] PRIMARY KEY CLUSTERED 
(
	[idConsumo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[empresa]    Script Date: 28/11/2019 10:45:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[empresa](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombreFiscal] [varchar](100) NULL,
	[nif] [varchar](100) NULL,
	[lat] [varchar](100) NULL,
	[lng] [varchar](100) NULL,
	[img] [varchar](100) NULL,
	[email] [varchar](100) NULL,
	[passwordUno] [varchar](100) NULL,
	[passwordDos] [varchar](100) NULL,
	[telefono] [int] NULL,
	[direccion] [varchar](200) NULL,
	[role] [int] NULL,
 CONSTRAINT [PK_empresa] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ofertas]    Script Date: 28/11/2019 10:45:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ofertas](
	[idOferta] [int] IDENTITY(1,1) NOT NULL,
	[descripcion] [varchar](1000) NULL,
	[precio] [int] NULL,
	[cantidad] [int] NULL,
	[caducidad] [date] NULL,
	[img] [varchar](200) NULL,
	[idEmpresa] [int] NULL,
 CONSTRAINT [PK_ofertas] PRIMARY KEY CLUSTERED 
(
	[idOferta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[consumos]  WITH CHECK ADD  CONSTRAINT [FK_consumos_clientes] FOREIGN KEY([idUser])
REFERENCES [dbo].[clientes] ([id])
GO
ALTER TABLE [dbo].[consumos] CHECK CONSTRAINT [FK_consumos_clientes]
GO
ALTER TABLE [dbo].[consumos]  WITH CHECK ADD  CONSTRAINT [FK_consumos_ofertas] FOREIGN KEY([idOferta])
REFERENCES [dbo].[ofertas] ([idOferta])
GO
ALTER TABLE [dbo].[consumos] CHECK CONSTRAINT [FK_consumos_ofertas]
GO
ALTER TABLE [dbo].[ofertas]  WITH CHECK ADD  CONSTRAINT [FK_ofertas_empresa] FOREIGN KEY([idEmpresa])
REFERENCES [dbo].[empresa] ([id])
GO
ALTER TABLE [dbo].[ofertas] CHECK CONSTRAINT [FK_ofertas_empresa]
GO
